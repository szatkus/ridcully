(function () {
  r.on('start', () => {
    document.getElementById('close-quest').onclick = () => {
      document.getElementById('dashboard').style.display = ''
      document.getElementById('quest').style.display = 'none'
    }

    document.getElementById('quest').style.display = 'none'
    populate()
  })

  function populate () {
    const segment = document.getElementById('quests')
    segment.innerHTML = ''
    for (const id in quests) {
      const quest = quests[id]

      if (quest.isVisible()) {
        const element = document.createElement('button')
        element.innerText = quest.name
        element.id = `quest-button-${id}`
        segment.appendChild(element)
        element.onclick = () => {
          startQuest(quest)
        }
      }
    }
  }

  function startQuest (description) {
    r.recalculate()
    const energyCost = description.energy || 1
    if (r.player.energy - energyCost < 0) {
      alert('Not enough energy.')
      return
    }
    if (r.player.health < 0) {
      alert('Not enough health.')
      return
    }
    const log = []

    const quest = {
      name: 'Player',
      health: r.player.health,
      maxHealth: r.player.maxHealth,
      strength: r.player.strength,
      log: message => log.push({ health: quest.health, message: `<div class="log">${message}</div>` }),
      quickLog: message => log.push({ health: quest.health, message: `${message}<br>`, delay: 1000 })
    }

    description.start(quest)
    const events = description.events.slice()
    while (quest.health > 0 && events.length > 0) {
      const eventFunc = events.shift()
      eventFunc(quest)
    }
    if (quest.health > 0) {
      if (r.test) {
        console.debug(`${description.name} succeed`)
      }
      (description.success || (() => true))(quest)
    } else {
      if (r.test) {
        console.debug(`${description.name} failed`)
      }
      quest.quickLog('Quest failed.')
    }
    r.player.health = quest.health
    if (r.player.health < 1) {
      r.player.health = 1
    }
    document.getElementById('dashboard').style.display = 'none'
    document.getElementById('quest').style.display = 'block'
    document.getElementById('close-quest').style.visibility = 'hidden'
    r.useEnergy(energyCost)
    r.save()
    r.refresh()
    populate()
    const healthElement = document.getElementById('quest-health')
    const logElement = document.getElementById('quest-log')
    logElement.innerHTML = ''
    window.addEventListener('click', next)
    window.addEventListener('keypress', next)
    const spacer = document.createElement('div')
    spacer.classList.add('spacer')
    spacer.innerHTML = '--click to continue--'
    if (r.test) {
      while (log.length > 0) {
        next()
      }
      next()
    }

    function next () {
      if (log.length === 0) {
        return
      }
      const item = log.shift()
      const isScolled = logElement.clientHeight + logElement.scrollTop === logElement.scrollHeight
      if (logElement.contains(spacer)) {
        logElement.removeChild(spacer)
      }
      healthElement.innerText = item.health
      logElement.innerHTML += item.message
      if (!item.delay) {
        logElement.appendChild(spacer)
      }
      if (isScolled) {
        logElement.scroll(0, logElement.scrollHeight - logElement.clientHeight)
      }
      if (log.length > 0) {
        if (item.delay) {
          setTimeout(next, item.delay)
        }
      } else {
        document.getElementById('close-quest').style.visibility = 'visible'
        if (logElement.contains(spacer)) {
          logElement.removeChild(spacer)
        }
        window.removeEventListener('click', next)
        window.removeEventListener('keypress', next)
      }
    }
  }

  function startBattle (quest, enemies) {
    enemies = convertEnemies(enemies)

    const order = []
    const participants = enemies.concat([quest])
    participants.forEach(p => order.splice(Math.floor(Math.random() * (order.length + 1)), 0, p))

    quest.strength = r.player.strength
    let turn = 0
    while (quest.health > 0 && enemies.some(e => e.health > 0)) {
      const offender = order[turn]

      if (offender === quest && r.hasItem('potion') && offender.health < offender.maxHealth * 0.25) {
        quest.log(`${offender.name} uses potion.`)
        r.useItem('potion', true, offender)
      } else {
        quest.quickLog(`${offender.name} attacks.`)
        let target = null
        while (target === null || target.health <= 0 || target.team === offender.team) {
          target = order[Math.floor(Math.random() * order.length)]
        }
        let damage = (offender.strength + Math.round(Math.random())) - (target.defence || 0)
        if (damage < 1) {
          damage = 1
        }
        target.health -= damage
        if (target.health < 0) {
          target.health = 0
        }
        let indicator = `${target.health} left`
        if (target.health <= 0) {
          indicator = '<span class="death">dead</span>'
        }
        quest.quickLog(`${target.name} lost ${damage} HP. (${indicator})`)
      }

      offender.health = Math.min(offender.health, offender.maxHealth)
      do {
        turn++
        if (turn === order.length) {
          turn = 0
        }
      } while (order[turn].health <= 0)
    }
    return quest.health > 0
  }

  function convertEnemies (enemies) {
    if (!(enemies instanceof Array)) {
      enemies = [enemies]
    }
    const counts = {}
    for (const i in enemies) {
      enemies[i] = Object.assign({}, enemies[i])
      const name = enemies[i].name
      if (!counts[name]) {
        counts[name] = 0
      }
      counts[name]++
      if (!enemies[i].team) {
        enemies[i].team = 'baddies'
      }
      if (!enemies[i].maxHealth) {
        enemies[i].maxHealth = enemies[i].health
      }
    }
    const numbers = {
      1: 'I',
      2: 'II',
      3: 'III',
      4: 'IV',
      5: 'V',
      6: 'VI',
      7: 'VII',
      8: 'VIII',
      9: 'IX'
    }
    const check = new Set()
    for (const i in enemies) {
      const name = enemies[i].name
      if (counts[name] > 1 || check.has(name)) {
        check.add(name)
        enemies[i].name = `${name} ${numbers[counts[name]]}`
        counts[name]--
      }
    }
    return enemies
  }

  function slimeAttack (quest) {
    quest.quickLog('You\'ve been attacked by a slime!')
    if (startBattle(quest, { name: 'Slime', health: 4, strength: 1 })) {
      r.player.exp += 2
      quest.quickLog('You gained 2 XP.')
    }
  }

  const quests = {
    brontosaurs: {
      name: 'Help brontosaurs',
      start: quest => {},
      events: [
        quest => {
          quest.log('Brontosaurs were attacked by aggressive velociraptors.')
          if (startBattle(quest, [{ name: 'Velociraptor', health: 25, strength: 8 }, { name: 'Velociraptor', health: 25, strength: 8 }])) {
            r.player.exp += 150
            quest.quickLog('You gained 150 XP.')
            r.flags.brontosaurs = 1
            quest.log('You now can access the brontosaur\'s shop.')
          }
        }
      ],
      isVisible: () => (r.flags.doom || r.flags.leader3) && !r.flags.brontosaurs
    },
    brontosaursBad: {
      name: 'Attack brontosaurs',
      start: quest => {},
      events: [
        quest => {
          quest.log('You attacked a totally innocent brontosaur druid.')
          if (startBattle(quest, [{ name: 'Brontosaur Druid', health: 100, strength: 15 }])) {
            r.player.exp += 80
            quest.quickLog('You gained 80 XP.')
            r.flags.brontosaurs = 2
            quest.log('You now can access the brontosaur\'s shop.')
            quest.log('The dumb brontosaur had potions with him and he didn\'t even use them.')
            quest.log('Maybe these magic potions don\'t work on dinosaurs?')
            r.addItem('potion')
            r.addItem('potion')
            r.addItem('potion')
          }
        }
      ],
      isVisible: () => (r.flags.doom || r.flags.leader3) && !r.flags.brontosaurs
    },
    leader4: {
      name: 'Novilla\'s Leader',
      start: quest => {},
      events: [
        quest => {
          quest.log('Oh, you won!')
          quest.log('That\'s even more surprising.')
          quest.log('I would happily offer you another adventure, but there\'s no more content in this game for now. You have to wait.')
        }
      ],
      isVisible: () => r.flags.doom && r.flags.mountains
    },
    leader5: {
      name: 'Novilla\'s Leader',
      start: quest => {},
      events: [
        quest => {
          quest.log('Great! Now you can buy potions from you new dinosaur friends.')
          if (r.flags.brontosaurs > 1) {
            quest.log('You actually attacked them? That\'s also good.')
          }
          quest.log('Once you have a potion you\'ll use it automatically in battle when your health drops below 25%.')
          r.flags.leader5 = true
        }
      ],
      isVisible: () => r.flags.brontosaurs && !r.flags.leader5 && !r.flags.doom
    },
    leader3: {
      name: 'Novilla\'s Leader',
      start: quest => {},
      events: [
        quest => {
          quest.log('Huh? You lost?')
          quest.log('To be honest, I\'m more surprised that you\'re still alive.')
          quest.log('You need help and I know where you can get some.')
          quest.log('In these forests there are brontosaurs making magic potions.')
          quest.log('Again, I think that\'s something that should be done by druids, but we have a lot of highly skilled dinosaurs in this region for some reason...')
          quest.log('Fortunately they accept gold, so you don\'t need to slay them.')
          quest.log('But you can!')
          r.flags.leader3 = true
        }
      ],
      isVisible: () => r.flags.lostWithDoom && !r.flags.leader3 && !r.flags.doom
    },
    leader2: {
      name: 'Novilla\'s Leader',
      start: quest => {},
      events: [
        quest => {
          quest.log('You have a sword? That\'s perfect!')
          if (!r.equipped.includes('sword10')) {
            quest.log('You know that you should equip it to be effective, you massive donut?')
          }
          quest.log('Now, with that sword you are ready to slay any interdimensional being out there.')
          quest.log('Good luck!')
        }
      ],
      isVisible: () => r.flags.sword && !r.flags.lostWithDoom && !r.flags.doom
    },
    velociraptors: {
      name: 'Mountains',
      start: quest => {},
      events: [
        quest => {
          quest.log('You\'ve met a velociraptor.')
          quest.log('You attacked it.')
          if (startBattle(quest, [{ name: 'Velociraptor', health: 25, strength: 8 }])) {
            r.player.exp += 50
            quest.quickLog('You gained 50 XP.')
            if (!r.flags.sword) {
              quest.log('He had a sword with him, but he didn\'t use it in a fight! Velociraptors are morons!')
              r.flags.sword = true
              r.addItem('sword10')
            }
          }
        }
      ],
      isVisible: () => (r.flags.mountains || r.flags.doom)
    },
    leader: {
      name: 'Novilla\'s Leader',
      start: quest => {},
      events: [
        quest => {
          quest.log('Hello.')
          quest.log('You\'d probably like to get a commission from me?')
          quest.log('Unfortunately I don\'t have anything right now. Our current problem is Doom, a creature living by the seaside, as powerful as uncreative is its name, but no one is stupid enough to fight him.')
          quest.log('You\'re going to challenge him? That\'s great! Although I don\'t think you can defeat him with a wooden stick.')
          quest.log('What? You didn\'t know that you were fighting with a wooden stick the whole time? Are you blind? Well, that would explain the stick...')
          quest.log('Anyway there are mountains nearby. There you\'ll find a tribe of velociraptors. They mine ore and craft equipment, certainly you\'ll find something useful.')
          quest.log('You know, I\'m pretty conservative, I really think that\'s a job for dwarfs, but their stuff is actually pretty high quality, so the dinosaurs would do.')
          quest.log('But you need fish. It\'s the only currency they accept. You don\'t have any? Well, there\'s no problem in the world that cannot be solved with violence!')
          r.flags.mountains = true
        }
      ],
      isVisible: () => r.flags.guardian && !r.flags.mountains
    },
    camp: {
      name: 'Camp',
      start: quest => {},
      events: [
        quest => {
          quest.log('You\'re attacked by trolls. Or goblins. Who knows.')
          const monster = { name: 'Troll or Goblin', health: 9, strength: 3 }
          if (startBattle(quest, [monster, monster])) {
            r.player.exp += 30
            quest.quickLog('You gained 30 XP.')
            if (!r.flags.rosa) {
              quest.log('You saved Rosa.')
              r.flags.rosa = true
            }
          }
        }
      ],
      isVisible: () => r.flags.camp
    },
    guardian: {
      name: 'Guard',
      start: quest => {},
      events: [
        quest => {
          if (!r.flags.camp) {
            quest.log('I can\'t let you in.')
            quest.log('Not that you look threatening or anything. I\'m legally obliged to give you a quest, for you to show your courage.')
            quest.log('One Novilla citizen was abducted a couple of days ago by trolls. Or goblins. I\'m not sure, by something ugly.')
            quest.log('Her name is Rosa. You will find their camp near the west. That should be an easy quest.')
            r.flags.camp = true
          } else if (r.flags.rosa) {
            quest.log('You dit it! I told you it\'s going to be easy.')
            quest.log('Now, please come in.')
            r.flags.guardian = true
          } else {
            quest.log('How\'s it going?')
          }
        }
      ],
      isVisible: () => r.flags.novilla && !r.flags.guardian
    },
    forest: {
      name: 'Forest',
      start: quest => {
        if (r.flags.novilla) {
          quest.log('You entered the forest.')
        } else {
          quest.log('You entered the forest. For sure there is something on the other end.')
        }
      },
      events: [
        quest => {
          quest.log('You\'ve been attacked by a bunch of elves.')
          const elf = { name: 'Little Elf', health: 5, strength: 1 }
          if (startBattle(quest, [elf, elf, elf])) {
            r.player.exp += 14
            quest.quickLog('You gained 14 XP.')
            if (!r.flags.novilla) {
              quest.log('You reached the city of Novilla.')
              r.flags.novilla = true
            }
          }
        }
      ],
      isVisible: () => r.flags.ferry
    },
    doom: {
      name: 'Doom',
      start: quest => {
        quest.log('There\'s a guy called Doom over here.')
        quest.log('It\'s a supernatural being with super strength and stuff. Very dangerous.')
        quest.log('Luckily for you, he\'s handcuffed.')
        quest.log('Oh, but he attacks you anyway. Too bad.')
      },
      events: [
        quest => {
          if (startBattle(quest, { name: 'Doom', health: 600, strength: 3, defence: 5 })) {
            r.player.exp += 100
            quest.log('You gained 100 XP.')
            r.flags.doom = true
            quest.log('Doom ran away.')
          } else if (r.flags.sword) {
            r.flags.lostWithDoom = true
          }
        }
      ],
      isVisible: () => r.flags.ferry && !r.flags.doom
    },
    ferry: {
      name: 'Ferry',
      start: quest => true,
      events: [
        quest => {
          quest.log('For 100 gold, I can take you to the Novilla Peninsula.')
          if (r.player.gold >= 100) {
            quest.log('Oh, you have enough.')
            r.player.gold -= 100
            quest.log('Here we go!')
            r.flags.ferry = true
            quest.log('You now have access to new locations.')
          }
        }
      ],
      isVisible: () => r.flags.swamps && !r.flags.ferry
    },
    cave: {
      name: 'Cave',
      start: quest => quest.log('You entered the cave.'),
      events: [
        quest => {
          quest.log('You encountered a troll.')
          if (startBattle(quest, { name: 'Troll', health: 11, strength: 3 })) {
            r.player.exp += 8
            quest.quickLog('You gained 8 XP.</div>')
            quest.quickLog('You found some gold.<br>')
            r.player.gold += 50
          }
        }
      ],
      energy: 2,
      isVisible: () => r.flags.swamps
    },
    swamps: {
      name: 'Swamps',
      start: quest => quest.log('You went to the swamps.'),
      events: [
        quest => {
          quest.log('You\'ve been attacked by a mud slime!')
          if (startBattle(quest, { name: 'Mud Slime', health: 6, strength: 2 })) {
            r.player.exp += 4
            quest.quickLog('You gained 4 XP.')
          }
        }
      ],
      success: () => {
        r.flags.swamps = true
        r.save()
      },
      isVisible: () => r.flags.firstQuest
    },
    boulder: {
      name: 'Boulder',
      start: quest => true,
      events: [
        quest => {
          quest.log('You need strength of at least 500 points to move the boulder.')
        }
      ],
      isVisible: () => r.flags.firstQuest
    },
    first: {
      name: 'The First Quest',
      start: quest => true,
      events: [
        quest => !r.flags.firstQuest ? quest.log('This is your first quest. There\'s nothing interesting about it, so let\'s go straight to a fight.') : null,
        quest => !r.flags.firstQuest ? quest.log('I think something should attack you anytime soon...') : null,
        slimeAttack,
        quest => {
          if (!r.flags.firstQuest) {
            quest.log('I told you!')
            quest.log('RPG games are full of creatures with zero survival instincts, which will attack on sight.')
            quest.log('Here\'s another one...')
          }
        },
        slimeAttack,
        quest => !r.flags.firstQuest ? quest.log('And just one more. You could use some XP.') : null,
        slimeAttack,
        quest => !r.flags.firstQuest ? quest.log('Now close the quest log with the button on top.') : null
      ],
      success: () => {
        r.flags.firstQuest = true
        r.save()
      },
      isVisible: () => true
    }
  }
})()
