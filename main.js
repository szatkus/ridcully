const r = {
  energyRecoveryRate: 5 * 60 * 1000,
  healthRecoveryRate: 10 * 1000,
  recalculate: () => {
    r.player.maxHealth = 12
    r.player.strength = 3
    r.trigger('recalculate')
  },
  now: () => Math.floor(new Date() / 1000) * 1000
}

r.start = () => {
  r.load()
  r.recalculate()
  r.save()
  r.refresh()

  r.trigger('start')
}

window.onload = () => r.start()

navigator.serviceWorker.register('service.js', { scope: '/novilla/' })
