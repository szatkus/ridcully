(function () {
  r.on('start', () => {
    populate()
  })

  r.addItem = (key) => {
    if (!items[key].multiple && r.hasItem(key)) {
      return
    }
    r.inventory.push(key)
    populate()
  }

  r.hasItem = (key) => {
    for (const item of r.inventory) {
      if (item === key) {
        return true
      }
    }
    return false
  }

  r.useItem = (key, inBattle, context) => {
    for (const i in r.inventory) {
      if (r.inventory[i] === key) {
        if (inBattle) {
          items[key].battleAction(context)
          r.inventory.splice(i, 1)
          populate()
          return
        }
      }
    }
    throw new Error(`Character has no ${key} item`)
  }

  r.on('refresh', () => {
    document.getElementById('inventory-tab').style.display = r.flags.sword ? 'inline' : 'none'
  })

  r.on('recalculate', () => {
    for (const id of r.inventory) {
      if (r.equipped.includes(id)) {
        const item = items[id]
        if (item.strength) {
          r.player.strength += item.strength
        }
      }
    }
  })

  function populate () {
    const list = document.getElementById('inventory')
    list.innerHTML = ''
    for (const id of r.inventory) {
      const item = items[id]
      const element = document.createElement('button')
      const indicator = r.equipped.includes(id) ? '*' : ''
      element.id = `item-${id}`
      element.innerText = item.name + indicator
      element.onclick = function () {
        if (item.equipment) {
          for (const id of r.equipped) {
            if (items[id].type === item.type) {
              unequip(id)
            }
          }
          equip(id)
          r.recalculate()
          r.save()
          populate()
        }
      }
      list.appendChild(element)
    }
  }

  function unequip (id) {
    const index = r.equipped.indexOf(id)
    r.equipped.splice(index, 1)
  }

  function equip (id) {
    if (!r.equipped.includes(id)) {
      r.equipped.push(id)
    }
  }

  const items = {
    sword10: {
      name: 'Sword +10',
      type: 'weapon',
      strength: 10,
      equipment: true
    },
    potion: {
      name: 'Potion',
      type: 'battle',
      multiple: true,
      battleAction: character => {
        character.health += 50
      }
    }
  }
})()
