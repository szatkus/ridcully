00:20 First look
00:30 First quest
00:50 Check out stats
01:20 Second quest (6/2)
01:40 Third quest (15/5)
01:50 Ferry
02:10 Fight with super strong boss (1000/1)
02:50 Some grinding
03:20 Forest ()
04:00 Talk with guardians
05:00 Rescue Rosa
05:30 Get info about mountains
06:30 Fail and grind
07:00 Velociraptors and items
08:00 Fight with super strong boss again
08:30 Brontosaur's shop
10:00 Equip and defeat super strong boss
