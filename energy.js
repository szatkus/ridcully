(function () {
  r.useEnergy = (energyCost) => {
    r.player.energy -= energyCost
    r.player.lastEnergyUse = r.now()
    r.flags.energyUsed = true
  }

  r.on('refresh', refresh)

  r.on('recalculate', recalculate)

  r.on('start', () => {
    document.getElementById('relaxation-room').onclick = () => {
      if (r.player.lastEnergyUse) {
        r.player.lastEnergyUse -= 60000
        r.recalculate()
        r.save()
        r.refresh()
        this.blur()
      }
    }
  })

  r.on('tick', () => {
    if (r.player.lastEnergyUse) {
      recalculate()
      refresh()
    }
  })

  function recalculate () {
    if (r.player.lastEnergyUse) {
      const delta = new Date() - r.player.lastEnergyUse
      const regain = Math.floor(delta / r.energyRecoveryRate)
      r.player.energy += regain
      if (r.player.energy >= r.player.maxEnergy) {
        r.player.energy = r.player.maxEnergy
        delete r.player.lastEnergyUse
      } else {
        r.player.lastEnergyUse += r.energyRecoveryRate * regain
      }
    }
  }

  function refresh () {
    r.indicate('energy', `${r.player.energy}/${r.player.maxEnergy}`, 0, 0, 255)
    if (r.player.lastEnergyUse) {
      document.getElementById('energy-counter').innerText = `(${r.formatTime(r.player.lastEnergyUse + r.energyRecoveryRate - new Date())})`
    } else {
      document.getElementById('energy-counter').innerText = ''
    }
    document.getElementById('energy-bar').style.display = r.flags.energyUsed ? 'inline' : 'none'
  }
})()
