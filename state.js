r.save = () => {
  localStorage.setItem('save', JSON.stringify({
    player: r.player,
    flags: r.flags,
    skills: r.skills,
    inventory: r.inventory,
    equipped: r.equipped,
    tasks: r.tasks
  }))
}

r.load = () => {
  const state = JSON.parse(localStorage.getItem('save')) || {}
  r.player = state.player
  r.flags = state.flags || {}
  r.skills = state.skills || []
  r.inventory = state.inventory || []
  r.equipped = state.equipped || []
  r.tasks = state.tasks || []
  if (!r.player) {
    r.player = {
      maxHealth: 12,
      health: 12,
      strength: 3,
      gold: 0,
      exp: 0,
      maxEnergy: 100,
      energy: 100
    }
  }
}
