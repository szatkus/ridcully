/* eslint eqeqeq: 0 */
(async function () {
  const realSetTimeout = setTimeout
  const realSetInterval = setInterval
  const realMathRandom = Math.random

  if (location.search === '?test') {
    r.test = true
    const backup = localStorage.getItem('save')
    localStorage.setItem('backup', backup)
    window.onunload = () => {
      localStorage.setItem('save', backup)
    }
    localStorage.removeItem('save')
    const timers = []
    window.setTimeout = (func) => {
      const id = realSetTimeout(func, 1)
      timers.push(id)
      return id
    }
    window.setInterval = (func) => {
      const id = realSetInterval(func, 1)
      timers.push(id)
      return id
    }
    const randoms = [0.25, 0, 0.75, 0.9999999, 0.5]
    let i = 0
    Math.random = (func) => {
      i++
      if (i >= randoms.length) {
        i = 0
      }
      return randoms[i]
    }
    const states = []
    try {
      await assertEqual('health', '12/12')
      await assertEqual('strength', '3')
      await assertEqual('exp', '0/3')
      await assertEqual('level', '0 (0)')

      await click('quest-button-first')
      await closeQuest()
      await assertInvisible('gold-container')
      await assertGreaterThanZero('exp')
      await click('relaxation-room')
      await assertStartsWith('energy-counter', '(0:03:')
      await assertGreaterThanZero('level')

      await click('quest-button-swamps')
      await closeQuest()
      await assertEqual('level', '2 (4)')
      await click('skills-tab')
      await click('skill-health1')
      await click('skill-health2')
      await assertEqual('health', '14/14')
      await click('quests-tab')

      await click('quest-button-cave')
      await closeQuest()
      await click('quest-button-ferry')
      await closeQuest()
      await assertInvisible('quest-button-forest')
      await click('quest-button-cave')
      await closeQuest()
      await click('quest-button-cave')
      await closeQuest()
      await assertGreaterThanZero('gold')
      await startQuest('ferry')
      await startQuest('doom')
      await getVisibleElement('quest-button-doom')

      await click('skills-tab')
      await click('skill-strength1')
      await click('skill-health3')
      await click('skill-health4')
      await click('quests-tab')
      await startQuest('forest')
      await startQuest('forest')
      await startQuest('guardian')
      await buySkills('strength2', 'health101')
      await startQuest('camp')
      await startQuest('camp')
      await startQuest('guardian')
      states.push(localStorage.getItem('save'))
      await startQuest('leader')
      await buySkills('strength3', 'strength4', 'health5', 'health8')
      await startQuest('velociraptors')
      await startQuest('velociraptors')
      await click('inventory-tab')
      await click('item-sword10')
      await click('quests-tab')
      await buySkills('health9')
      await startQuest('velociraptors')
      await startQuest('velociraptors')
      await startQuest('velociraptors')
      await buySkills('strength101', 'strength5', 'health6', 'health7', 'health10', 'health11', 'health12')
      await startQuest('leader2')
      await startQuest('doom')
      await startQuest('leader3')
      await startQuest('brontosaurs')
      await startQuest('leader5')
      await startQuest('cave')
      await startQuest('cave')
      await click('shops-tab')
      await click('shop-item-potion')
      await click('quests-tab')
      await startQuest('velociraptors')
      await buySkills('strength6', 'health102', 'strength7', 'strength102')
      await startQuest('doom')
      await startQuest('leader4')

      // let's defeat Doom first
      localStorage.setItem('save', states.pop())
      r.start()
      await startQuest('camp')
      await buySkills('strength3', 'strength4', 'health5', 'health8')
      for (let i = 0; i < 30; i++) {
        await startQuest('camp')
      }
      await buySkills('strength101', 'strength5', 'health6', 'health7', 'health10', 'health11', 'health12')
      await buySkills('strength6', 'health000', 'strength102', 'strength7')
      await startQuest('doom')
      await startQuest('leader')
      await startQuest('leader4')
      await startQuest('brontosaursBad')
      await click('shops-tab')
      await assertEqual('shop-item-potion', 'Potion (200)')
      await click('quests-tab')

      console.log('All good!')
    } finally {
      window.setTimeout = realSetTimeout
      window.setInterval = realSetInterval
      Math.random = realMathRandom
    }
  }

  async function getVisibleElement (name) {
    let element = null
    for (let tries = 10; tries > 0 && (element == null || element.getBoundingClientRect().width === 0 || element.style.visibility === 'hidden'); tries--) {
      element = document.getElementById(name)
      await sleep()
    }
    if (!element) {
      throw new Error(`Cannot find #${name} element`)
    }
    if (element.getBoundingClientRect().width === 0 || element.style.visibility === 'hidden') {
      throw new Error(`Element ${name} is invisible`)
    }
    return element
  }

  async function closeQuest () {
    await click('close-quest')
  }

  async function buySkills (...skills) {
    await click('skills-tab')
    for (const name of skills) {
      await click('skill-' + name)
    }
    await click('quests-tab')
  }

  async function click (name) {
    await sleep()
    const element = await getVisibleElement(name)
    element.click()
  }

  function sleep (time = 5) {
    return new Promise((resolve) => {
      realSetTimeout(() => {
        resolve()
      }, time)
    })
  }

  async function assertInvisible (name) {
    const element = document.getElementById(name)
    if (!element) {
      return
    }
    if (element.getBoundingClientRect().width > 0 && element.style.visibility !== 'hidden') {
      throw new Error(`Element ${name} should be invisible`)
    }
  }

  async function assertEqual (name, expected) {
    const value = (await getVisibleElement(name)).innerText
    if (value != expected) {
      throw new Error(`${name} value is ${value}, should be ${expected}`)
    }
  }

  async function assertGreaterThan (name, expected) {
    const value = parseInt((await getVisibleElement(name)).innerText, 10)
    if (value <= expected) {
      throw new Error(`${name} value is ${value}, should be greater than ${expected}`)
    }
  }

  async function assertGreaterThanZero (name) {
    await assertGreaterThan(name, 0)
  }

  async function assertStartsWith (name, expected) {
    const value = (await getVisibleElement(name)).innerText
    if (value.indexOf(expected) !== 0) {
      throw new Error(`${name} value is ${value}, should start with ${expected}`)
    }
  }

  async function startQuest (name) {
    await click('quest-button-' + name)
    await closeQuest()
  }
})()
