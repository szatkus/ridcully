(function () {
  r.on('start', () => {
    populate()
  })

  r.on('refresh', () => {
    const level = getLevel()
    const skillPoints = getSkillPoints() - getSpentPoints()
    r.indicate('strength', r.player.strength, 0, 255, 0)
    if (r.inventory.length > 0) {
      document.getElementById('strength-label').innerText = 'Attack'
    }
    document.getElementById('exp').innerText = `${r.player.exp}/${getRequiredExp(level)}`
    document.getElementById('level').innerText = `${level} (${skillPoints})`
  })

  function getRequiredExp (level) {
    if (level === 0) {
      return 3
    }
    if (!level) {
      level = getLevel()
    }
    return 3 + Math.ceil(Math.E * level * (level + 1) * Math.pow(1.00109, level * level))
  }

  function getLevel () {
    let level = 0
    while (getRequiredExp(level) <= r.player.exp) {
      level++
    }
    return level
  }

  function getSkillPoints (level) {
    if (!level) {
      level = getLevel()
    }
    return level * level
  }

  function getSpentPoints () {
    return r.skills.map(s => skills[s].cost).reduce((a, v) => a + v, 0)
  }

  function populate () {
    const segment = document.getElementById('skills')
    segment.innerHTML = ''
    for (const key in skills) {
      const skill = skills[key]
      if (!r.skills.includes(key) && skill.deps.every(x => r.skills.includes(x))) {
        const element = document.createElement('button')
        element.id = `skill-${key}`
        element.innerText = `${skill.name} (${skill.cost})`
        segment.appendChild(element)
        element.onclick = () => {
          if (getSpentPoints() + skill.cost > getSkillPoints()) {
            alert('You don\'t have enough skill points.')
            return
          }
          r.skills.push(key)
          r.recalculate()
          r.save()
          populate()
          r.refresh()
          if (r.test) {
            console.debug(key)
          }
        }
      }
    }
  }

  r.on('recalculate', () => {
    for (const name of r.skills) {
      const skill = skills[name]
      if (skill.health) {
        r.player.maxHealth += skill.health
      }
      if (skill.strength) {
        r.player.strength += skill.strength
      }
    }
  })

  const skills = {
    health1: {
      name: 'Health +1',
      health: 1,
      deps: [],
      cost: 1
    },
    health2: {
      name: 'Health +1',
      health: 1,
      deps: ['health1'],
      cost: 1
    },
    health3: {
      name: 'Health +1',
      health: 1,
      deps: ['health2'],
      cost: 1
    },
    health4: {
      name: 'Health +1',
      health: 1,
      deps: ['health3'],
      cost: 1
    },
    strength1: {
      name: 'Strength +1',
      strength: 1,
      deps: ['health1'],
      cost: 4
    },
    strength2: {
      name: 'Strength +1',
      strength: 1,
      deps: ['strength1'],
      cost: 4
    },
    health101: {
      name: 'Health +3',
      health: 3,
      deps: ['strength2'],
      cost: 4
    },
    strength3: {
      name: 'Strength +1',
      strength: 1,
      deps: ['strength2'],
      cost: 4
    },
    strength4: {
      name: 'Strength +2',
      strength: 2,
      deps: ['strength3'],
      cost: 9
    },
    strength5: {
      name: 'Strength +2',
      strength: 2,
      deps: ['strength4'],
      cost: 9
    },
    strength6: {
      name: 'Strength +3',
      strength: 3,
      deps: ['strength5'],
      cost: 14
    },
    health102: {
      name: 'Health +12',
      health: 12,
      deps: ['strength6'],
      cost: 11
    },
    strength7: {
      name: 'Strength +3',
      strength: 3,
      deps: ['strength6'],
      cost: 14
    },
    health5: {
      name: 'Health +2',
      health: 2,
      deps: ['health4'],
      cost: 2
    },
    health6: {
      name: 'Health +3',
      health: 3,
      deps: ['health5'],
      cost: 4
    },
    health7: {
      name: 'Health +3',
      health: 3,
      deps: ['health6'],
      cost: 4
    },
    health8: {
      name: 'Health +4',
      health: 4,
      deps: ['health4'],
      cost: 4
    },
    strength101: {
      name: 'Strength +1',
      strength: 1,
      deps: ['health8'],
      cost: 3
    },
    health9: {
      name: 'Health +5',
      health: 5,
      deps: ['health8'],
      cost: 5
    },
    health10: {
      name: 'Health +6',
      health: 6,
      deps: ['health8'],
      cost: 8
    },
    health11: {
      name: 'Health +10',
      health: 10,
      deps: ['health10'],
      cost: 13
    },
    health000: {
      name: 'Health +100',
      health: 100,
      deps: ['health10'],
      cost: 110
    },
    health12: {
      name: 'Health +15',
      health: 15,
      deps: ['health10'],
      cost: 18
    },
    strength102: {
      name: 'Strength +6',
      strength: 6,
      deps: ['health11'],
      cost: 27
    },
    strength8: {
      name: 'Strength +10',
      strength: 10,
      deps: ['strength7'],
      cost: 43
    }
  }
})()
