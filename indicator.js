r.indicate = (id, content, red, green, blue) => {
  const element = document.getElementById(id)
  if (element.textContent !== content.toString() && element.textContent.trim() !== '') {
    const timeout = setInterval(() => {
      if (red > 0) {
        red--
      }
      if (green > 0) {
        green--
      }
      if (blue > 0) {
        blue--
      }
      element.style.color = `rgb(${red}, ${green}, ${blue})`
      if (red === green && green === blue && blue === 0) {
        clearTimeout(timeout)
      }
    }, 10)
  }
  element.textContent = content
}
