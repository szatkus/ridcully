(function () {
  r.on('start', () => {
    for (const task of r.tasks) {
      watchTask(task)
    }
    setInterval(() => r.trigger('tick'), 100)
  })

  r.startTask = (group, duration, data) => {
    const task = {
      endDate: r.now() + duration * 1000,
      group,
      data
    }
    r.tasks.push(task)
    watchTask(task)
  }

  r.isBusy = (group) => {
    for (const task of r.tasks) {
      if (task.group === group) {
        return true
      }
    }
    return false
  }

  r.getTask = (group) => {
    for (const task of r.tasks) {
      if (task.group === group) {
        return task
      }
    }
  }

  r.getRemainingTime = (group) => {
    const delta = r.getTask(group).endDate - new Date()
    return r.formatTime(delta)
  }

  r.finishTask = (task) => {
    r.tasks.splice(r.tasks.indexOf(task), 1)
  }

  function watchTask (task) {
    const delta = task.endDate - new Date()
    if (delta > 0) {
      setTimeout(() => {
        r.trigger('done', task)
      }, delta)
    } else {
      r.trigger('done', task)
    }
  }
})()
