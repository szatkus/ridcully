(function () {
  r.on('start', () => {
    document.getElementById('quests-tab').onclick = () => {
      for (const tab of document.querySelectorAll('.segment')) {
        tab.style.display = 'none'
      }
      document.getElementById('quests').style.display = ''
    }
    document.getElementById('skills-tab').onclick = () => {
      for (const tab of document.querySelectorAll('.segment')) {
        tab.style.display = 'none'
      }
      document.getElementById('skills').style.display = ''
    }
    document.getElementById('inventory-tab').onclick = () => {
      for (const tab of document.querySelectorAll('.segment')) {
        tab.style.display = 'none'
      }
      document.getElementById('inventory').style.display = ''
    }
    document.getElementById('shops-tab').onclick = () => {
      for (const tab of document.querySelectorAll('.segment')) {
        tab.style.display = 'none'
      }
      document.getElementById('shops').style.display = ''
    }
  })
  r.refresh = () => {
    document.getElementById('gold').innerText = r.player.gold
    document.getElementById('gold-container').style.display = r.flags.swamps ? 'inline' : 'none'

    document.getElementById('relaxation-room').style.display = r.flags.energyUsed ? 'inline' : 'none'

    document.getElementById('tabs').style.display = r.player.exp ? 'inline' : 'none'

    r.trigger('refresh')
  }

  r.formatTime = (delta) => {
    const hours = Math.floor(delta / (60 * 60 * 1000))
    delta = delta % (60 * 60 * 1000)
    let minutes = Math.floor(delta / (60 * 1000))
    if (minutes < 10) {
      minutes = '0' + minutes
    }
    delta = delta % (60 * 1000)
    let seconds = Math.floor(delta / 1000)
    if (seconds < 10) {
      seconds = '0' + seconds
    }
    return `${hours}:${minutes}:${seconds}`
  }
})()
