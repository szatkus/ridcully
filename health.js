r.on('recalculate', recalculate)

r.on('refresh', refresh)

r.on('tick', () => {
  if (r.player.lastDamage) {
    recalculate()
    refresh()
  }
})

function recalculate() {
  if (r.player.lastDamage) {
    const delta = new Date() - r.player.lastDamage
    const regain = Math.floor(delta / r.healthRecoveryRate)
    r.player.health += regain
    if (r.player.health >= r.player.maxHealth) {
      r.player.health = r.player.maxHealth
      delete r.player.lastDamage
    } else {
      r.player.lastDamage += r.healthRecoveryRate * regain
    }
  }
}

function refresh() {
  if (!r.player.lastDamage && r.player.health < r.player.maxHealth) {
    r.player.lastDamage = r.now()
    r.save()
  }
  r.indicate('health', `${r.player.health}/${r.player.maxHealth}`, 255, 0, 0)
  if (r.player.lastDamage) {
    document.getElementById('health-counter').innerText = `(${r.formatTime(r.player.lastDamage + r.healthRecoveryRate - new Date())})`
  } else {
    document.getElementById('health-counter').innerText = ''
  }
}
