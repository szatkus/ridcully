(function () {
  const eventTarget = new EventTarget()
  r.on = (eventName, listener) => {
    eventTarget.addEventListener(eventName, listener)
  }
  r.trigger = (eventName, data) => {
    const event = new CustomEvent(eventName, { detail: data })
    eventTarget.dispatchEvent(event)
  }
})()
