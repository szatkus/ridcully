(function () {
  let activeShop

  r.on('refresh', () => {
    document.getElementById('shops-tab').style.display = r.flags.brontosaurs ? 'inline' : 'none'
    const list = document.getElementById('shops-list')
    list.innerHTML = ''
    for (const key in shops) {
      const shop = shops[key]
      if (shop.isVisible()) {
        const element = document.createElement('button')
        element.innerText = shop.name
        element.onclick = () => showShop(shop)
        list.appendChild(element)
      }
    }
    if (!activeShop) {
      showShop(shops[Object.keys(shops)[0]])
    } else {
      showShop(activeShop)
    }
  })

  r.on('done', event => {
    if (event.detail.data === 'potion') {
      r.addItem('potion')
      r.finishTask(event.detail)
      r.save()
      r.refresh()
    }
  })

  r.on('tick', () => {
    refreshTimes()
  })

  function showShop (shop) {
    const list = document.getElementById('shops-item-list')
    list.style.display = 'block'
    list.innerHTML = ''
    activeShop = shop
    const shopList = document.getElementById('shops-list')
    shopList.style.display = 'none'
    const backButton = document.createElement('button')
    backButton.onclick = function () {
      list.style.display = 'none'
      shopList.style.display = 'block'
    }
    backButton.textContent = 'Shop list'
    list.appendChild(backButton)
    for (const key in shop.items) {
      const item = shop.items[key]
      const element = document.createElement('button')
      element.id = `shop-item-${key}`

      const cost = shop.getCost(item)
      list.appendChild(element)
      if (!r.isBusy(shop.name)) {
        element.onclick = () => {
          if (r.player.gold >= cost) {
            r.player.gold -= cost
            r.startTask(shop.name, item.duration, key)
            r.save()
            r.refresh()
          }
        }
      }
    }
    refreshTimes()
  }

  function refreshTimes () {
    for (const key in activeShop.items) {
      const item = activeShop.items[key]
      const element = document.getElementById(`shop-item-${key}`)
      let timer = ''
      if (r.isBusy(activeShop.name) && r.getTask(activeShop.name).data === key) {
        timer = r.getRemainingTime(activeShop.name)
      }
      element.innerText = `${item.name} (${activeShop.getCost(item)}) ${timer}`
    }
  }

  const shops = {
    brontosaurs: {
      name: 'Brontosaurs\' Shop',
      items: {
        potion: {
          name: 'Potion',
          duration: 180,
          cost: 100
        }
      },
      isVisible: () => r.flags.brontosaurs,
      getCost: item => item.cost * r.flags.brontosaurs
    }
  }
})()
